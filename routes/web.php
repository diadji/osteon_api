<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', function () use ($router) {
	return view("home");
});

$router->get('/api/doctor/{token}/infos-is-validated', 'DoctorController@infosIsValidated');
$router->get('/api/demandes-rdv/all', 'AppointmentController@getAll');
$router->post('/api/doctor-register', 'UserController@createDoctorAccount');
$router->post('/api/signUp', 'UserController@signUp');
$router->post('/api/doctor/add-diplome', 'DoctorController@addDiplome');

$router->post('/api/patient/make-appointment', 'PatientController@makeAppointment');
$router->post('/api/patient-register', 'UserController@createPatientAccount');

$router->get("/administrateur", function () {
	return view("infos-medecin");
});

$router->get('/medecins/all', 'DoctorController@allMedecins');
