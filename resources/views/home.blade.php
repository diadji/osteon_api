<!DOCTYPE html>
<html>
<head>
    <title>Osteon App</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body style="background: black;">
<div id="particles-js"  class="wrapper wrapper-full-page" style="">
    <div id="content-home">
        <h2>Bienvenue dans <span>Osteon</span></h2>
    </div>
</div>

<script src="js/assets/particles.min.js"></script>
<script>
    particlesJS.load('particles-js',
    "/js/assets/particles.json",function(){
      console.log("ok")
    })
  </script>

</body>
</html>
