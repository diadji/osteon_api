<!DOCTYPE html>
<html>
    <head>
        <title>
            Administrateur
        </title>
        <link href="css/semantic.min.css" rel="stylesheet" type="text/css">
            <script src="js/assets/semantic.min.js" type="text/javascript">
            </script>
            @yield('header')
        </link>
    </head>
    <body>
        <div class="ui secondary pointing menu">
            <div style="padding:0.5%;">
                <img height="50px" src="images/logo.png" width="150px">
                </img>
            </div>
        </div>
        <div class="ui grid">
            <div class="three wide column">
                <div class="ui secondary vertical menu">
                    <a class="active item">
                        Liste des utilisateurs
                    </a>
                    <a class="item">
                        Dossiers ostéopathes
                    </a>
                    <a class="item">
                        Settings
                    </a>
                    <a class="item">
                        Settings
                    </a>
                    <a class="item">
                        Settings
                    </a>
                </div>
            </div>
            <div class="twelve wide column">

                @yield('content')
            </div>
        </div>
        @yield('script')
    </body>
</html>
