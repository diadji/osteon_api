var $ = require("jquery")
var moment = require("moment");

export function getAllMedecins() {
    $.getJSON("medecins/all").done(response => {
        response.forEach(function(doctor) {
            // statements
            console.log("ok")
            moment.locale("fr")
            var date = moment(doctor.register_date).format("dddd, Do MMMM  YYYY, à h:mm")
            var card = `<div class="card">
                        <div class="content">
                            <img class="right floated mini ui image" src="/images/doctor.svg">
                                <div class="header">
                                    ${doctor.first_name} ${doctor.last_name}
                                </div>
                                <div class="meta">
                                    Ostéopathe
                                </div>
                                <div class="description">
                                   Inscris le <strong>${date}</strong>
                                </div>
                            </img>
                        </div>
                        <div class="extra content">
                            <div class="ui two buttons">
                                <div class="ui basic green button">
                                    Approve
                                </div>
                                <div class="ui basic red button">
                                    Decline
                                </div>
                            </div>
                        </div>
                    </div>`;
            $("#doctor-list").append(card);
        });
    }).fail(error => {
        console.log(error);
    })
}
