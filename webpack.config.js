var path = require('path');

module.exports = {
    mode: 'development',
  entry: './public/js/custom/main.js',
   output: {
    path: path.resolve(__dirname, 'public/js/dist'),
    filename: 'bundle.js'
  }
};
