<?php

/**
 *
 */
namespace App\Http\Controllers;

use App\Service\AppointmentService;

class AppointmentController extends Controller {

	public function makeAppointment(Request $request) {
		$result = array();
		try {
			$city = $request->input('data')['city'];

			$token = (new Parser())->parse((string) $request->input('data')['token']);

			$patientId = $token->getClaim('uid');

			$result['city'] = $city;

			$appointment = new Appointment();
			$appointment->setAdresse($city);
			$appointment->setIdPatient($patientId);
			//
			if (AppointmentService::createAppointment($appointment)) {
				$result['success'] = true;
			} else {
				$result['success'] = false;
			}
		} catch (\Exception $e) {
			$result['error'] = $e->getMessage();
			$result['success'] = false;
		}

		return response()->json($result);
	}

	public function getAll() {

		$result = array();
		$result = AppointmentService::findAll();

		return response()->json($result);
	}
}
