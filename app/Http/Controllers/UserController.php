<?php

namespace App\Http\Controllers;

use App\Model\Doctor;
use App\Model\Patient;
use App\Model\User;
use App\Service\JWTClass;
use App\Service\UserService;
use Illuminate\Http\Request;

class UserController extends Controller {
	// TODO user_id format
	public function createDoctorAccount(Request $request) {
		$email = $request->input('data')['email'];
		$password = $request->input('data')['password'];
		$lastName = $request->input('data')['lastName'];
		$firstName = $request->input('data')['firstName'];

		$response = array();

		$user = new Doctor();

		$user->setEmail($email);
		$user->setPassword(sha1($password));
		$user->setFirstName($firstName);
		$user->setLastName($lastName);
		$user->setUserType('doctor');

		try {
			$id = UserService::createUser($user);

			if ($id !== null) {
				$response['success'] = true;
			}else{
				$response['success'] = false;
			}
		} catch (\Exception $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}

		return response()->json($response);
	}

	public function createPatientAccount(Request $request) {
		$email = $request->input('data')['email'];
		$password = $request->input('data')['password'];
		$lastName = $request->input('data')['lastName'];
		$firstName = $request->input('data')['firstName'];
		// $sexe = $request->input('data')['sexe'];
		// $age = $request->input('data')['age'];

		$patient = new Patient();
		$patient->setEmail($email);
		$patient->setFirstName($firstName);
		$patient->setLastName($lastName);
		$patient->setPassword(sha1($password));
		$patient->setUserType('patient');

		$response = array();
		try {
			$id = UserService::createUser($patient);

			if ($id !== null) {
				$response['success'] = true;
			}else{
				$response['success'] = false;
			}
		} catch (\Exception $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}

		return response()->json($response);
	}

	public function signUp(Request $request) {
		$email = $request->input('data')['email'];
		$password = $request->input('data')['password'];

		$result = array();

		try {
			$r = UserService::findUserByEmail($email, $password);

			if ($r['success']) {
				$user = UserService::findUserByEmail($email, $password);

				$result['user_type'] = $r['user']->user_type;
				$result['auth'] = true;

				$t = new JWTClass($r['user']->user_id);
				$result['token'] = $t->getToken();
			}else{
				$result['auth'] = false;
				$result['error_auth'] = true;
			}
		} catch (\Exception $e) {
			$result['auth'] = false;
			$result['error_auth'] = false;
			$result['server_error'] = $e->getMessage();
		}

		return response()->json($result);
	}

	private function getInstanceOfUser(Request $request): User{
		$email = $request->input('data')['email'];
		$password = $request->input('data')['password'];
		$lastName = $request->input('data')['lastName'];
		$firstName = $request->input('data')['firstName'];

		$user = new User();
		$user->setEmail($email);
		$user->setPassword($password);
		$user->setLastName($lastName);
		$user->setFirstName($firstName);

		return $user;
	}

}
