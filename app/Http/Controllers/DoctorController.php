<?php

namespace App\Http\Controllers;

use App\Helpers\HelperClass;
use App\Model\DiplomeAndCIN;
use App\Service\DiplomeAndCINService;
use App\Service\UserService;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;

/**
 *
 */
class DoctorController extends Controller {

	public function addDiplome(Request $request) {

		//$fileName = $request->file('image')->getClientOriginalName();
		$token = null;
		$result = array();
		$token = $request->input('token');

		//recuperation du diplome et de la carte d'identite du docteur
		if ($token != null) {
			$result['auth'] = true;

			if ($request->file("diplome")->isValid() && $request->file("identite")->isValid()) {

				$diplomeName = HelperClass::generateRandomString();
				$identiteName = HelperClass::generateRandomString();

				$diplomeName = $diplomeName . "." . $request->file("diplome")->guessClientExtension();
				$identiteName = $identiteName . "." . $request->file("identite")->guessClientExtension();

				try {
					$request->file("diplome")->move("storages/diplome-cin", $diplomeName);
					$request->file("identite")->move("storages/diplome-cin", $identiteName);

					$token = (new Parser())->parse((string) $token);

					$doctorId = $token->getClaim('uid');

					$dac = new DiplomeAndCIN();

					$dac->setDiplomeName($diplomeName);
					$dac->setCinName($identiteName);
					$dac->setDoctorId($doctorId);
					$dac->setValidated(0);

					$result['created'] = DiplomeAndCINService::create($dac);

					$result['success'] = true;
				} catch (\Exception $e) {
					$result['error_message'] = $e->getMessage();
					$result['success'] = false;
				}
			} else {
				$result['success'] = false;
				$result['invalid_photo'] = true;
			}
		} else {
			$result['auth'] = false;
		}

		return response()->json($result);
	}

	public function infosIsValidated($token) {
		$token = (new Parser())->parse((string) $token);

		$doctorId = $token->getClaim('uid');

		$result = null;
		$result = DiplomeAndCINService::findByIdDoctor($doctorId);

		$response = array();

		if ($result != null) {
			$response['exist'] = true;

			if ($result->validated == 0) {
				$response['validated'] = false;
			} else {
				$response['validated'] = true;
			}
		} else {
			$response['exist'] = false;
		}

		return response()->json($response);
	}

	public function allMedecins() {
		return response()->json(UserService::getAllMedecins());
	}
}
