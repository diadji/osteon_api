<?php

namespace App\Helpers;
/**
 *
 */
class HelperClass {

	public static function generateRandomString($length = 50, $charSet = '0123456789abcdefghijklmnopqrstuvwxyz@$ABCDEFGHIJKLMNOPQRSTUVWXYZ_') {
		$pieces = [];
		$max = mb_strlen($charSet, '8bit') - 1;

		for ($i = 0; $i < $length; ++$i) {
			$pieces[] = $charSet[rand(0, $max)];
		}

		return implode('', $pieces);
	}
}
