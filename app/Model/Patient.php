<?php
/**
 * Created by PhpStorm.
 * User: diadji
 * Date: 3/19/19
 * Time: 5:32 PM
 */

namespace App\Model;


class Patient extends User
{
    private $profession;
    private $age;
    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     */
    public function setProfession($profession): void
    {
        $this->profession = $profession;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }
}
