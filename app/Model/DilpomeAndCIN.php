<?php

namespace App\Model;

/**
 *
 */
class DiplomeAndCIN {
	private $id;
	private $diplomeName;
	private $cinName;
	private $doctorId;
	private $validated;
	private $createdAt;

	/**
	 * @return mixed
	 */
	public function getDiplomeName() {
		return $this->diplomeName;
	}

	/**
	 * @param mixed $diplomeName
	 *
	 * @return self
	 */
	public function setDiplomeName($diplomeName) {
		$this->diplomeName = $diplomeName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCinName() {
		return $this->cinName;
	}

	/**
	 * @param mixed $cinName
	 *
	 * @return self
	 */
	public function setCinName($cinName) {
		$this->cinName = $cinName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDoctorId() {
		return $this->doctorId;
	}

	/**
	 * @param mixed $doctorId
	 *
	 * @return self
	 */
	public function setDoctorId($doctorId) {
		$this->doctorId = $doctorId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getValidated() {
		return $this->validated;
	}

	/**
	 * @param mixed $validated
	 *
	 * @return self
	 */
	public function setValidated($validated) {
		$this->validated = $validated;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * @param mixed $createdAt
	 *
	 * @return self
	 */
	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return self
	 */
	public function setId($id) {
		$this->id = $id;

		return $this;
	}
}
