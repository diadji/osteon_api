<?php

namespace App\Model;

class Appointment {
	private $id;
	private $idPatient;
	private $idDoctor;
	private $adresse;
	private $date;
	private $confirm;
	private $price;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return self
	 */
	public function setId($id) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdPatient() {
		return $this->idPatient;
	}

	/**
	 * @param mixed $idPatient
	 *
	 * @return self
	 */
	public function setIdPatient($idPatient) {
		$this->idPatient = $idPatient;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdDoctor() {
		return $this->idDoctor;
	}

	/**
	 * @param mixed $idDoctor
	 *
	 * @return self
	 */
	public function setIdDoctor($idDoctor) {
		$this->idDoctor = $idDoctor;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * @param mixed $adresse
	 *
	 * @return self
	 */
	public function setAdresse($adresse) {
		$this->adresse = $adresse;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param mixed $date
	 *
	 * @return self
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getConfirm() {
		return $this->confirm;
	}

	/**
	 * @param mixed $confirm
	 *
	 * @return self
	 */
	public function setConfirm($confirm) {
		$this->confirm = $confirm;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param mixed $price
	 *
	 * @return self
	 */
	public function setPrice($price) {
		$this->price = $price;

		return $this;
	}
}
