<?php
namespace App\Model;

class User {
	private $id;
	private $lastName;
	private $firstName;
	private $email;
	private $password;
	private $registerDate;
	private $userType;

	/**
	 * @return mixed
	 */
	public function getUserType() {
		return $this->userType;
	}

	/**
	 * @param mixed $userType
	 */
	public function setUserType($userType): void{
		$this->userType = $userType;
	}

	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param mixed $lastName
	 */
	public function setLastName($lastName): void{
		$this->lastName = $lastName;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param mixed $firstName
	 */
	public function setFirstName($firstName): void{
		$this->firstName = $firstName;
	}

	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email): void{
		$this->email = $email;
	}

	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password): void{
		$this->password = $password;
	}

	public function getRegisterDate() {
		return $this->registerDate;
	}

	/**
	 * @param mixed $registerDate
	 */
	public function setRegisterDate($registerDate): void{
		$this->registerDate = $registerDate;
	}

	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void{
		$this->id = $id;
	}

}
