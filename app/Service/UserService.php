<?php

/**
 *
 */
namespace App\Service;

use Illuminate\Support\Facades\DB;

class UserService {

	public static function createUser($user) {

		return DB::table('users')->insertGetId(
			[
				'first_name' => $user->getFirstName(),
				'last_name' => $user->getLastName(),
				'email' => $user->getEmail(),
				'password' => $user->getPassword(),
				'user_type' => $user->getUserType(),
			]
		);
	}

	public static function findUserByEmail($email, $password) {
		$response = array();
		$response['success'] = false;

		$user = DB::table('users')
			->where('email', $email)
			->first();
		try {

			if ($user->password === sha1($password)) {
				$response['success'] = true;
				$response['user'] = $user;
			}

		} catch (\Exception $e) {
			$response['success'] = false;
		}

		return $response;
	}

	public static function getAllMedecins() {
		return DB::table('users')->where('user_type', 'doctor')->get();
	}
}
