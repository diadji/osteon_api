<?php
/**
 * Created by PhpStorm.
 * User: diadji
 * Date: 3/2/19
 * Time: 9:28 PM
 */

namespace App\Service;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class JWTClass
{
    private $userId;
    private const KEY = 'OSTEON1996';

    public function __construct($userId){
        $this->userId = $userId;
    }

    public function getToken(){
      $signer = new Sha256();

      $jwt = new JWTClass($this->userId);

      $token = (new Builder())->setIssuer('http://example.com') // Configures the issuer (iss claim)
        ->setAudience('http://example.org') // Configures the audience (aud claim)
        ->setId('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item
        ->setIssuedAt(time()) // Configures the time that the token was issued (iat claim)
        ->setNotBefore(time() + 60) // Configures the time that the token can be used (nbf claim)
        ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
        ->set('uid', $this->userId) // Configures a new claim, called "uid"
        ->sign($signer, self::KEY)
        ->getToken(); // Retrieves the generated token

      return strval($token);
    }
}
