<?php

namespace App\Service;

use App\Model\Appointment;
use Illuminate\Support\Facades\DB;

class AppointmentService {

	public static function createAppointment(Appointment $appointment) {
		return DB::table('demande_consultation')->insertGetId(
			[
				'id_patient' => $appointment->getIdPatient(),
				'adresse' => $appointment->getAdresse(),
				'price' => 35,
			]
		);
	}

	public static function findAll() {
		return DB::table('demande_consultation')->get();
	}
}
