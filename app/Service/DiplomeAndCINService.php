<?php
/**
 *
 */
namespace App\Service;

use App\Model\DiplomeAndCIN;
use Illuminate\Support\Facades\DB;

class DiplomeAndCINService {

	public static function create(DiplomeAndCIN $infos) {

		return DB::table("diplome_cin")->insert(
			[
				"diplome_name" => $infos->getDiplomeName(),
				"cin_name" => $infos->getCinName(),
				"doctor_id" => $infos->getDoctorId(),
				"validated" => $infos->getValidated(),
			]);
	}

	public static function findByIdDoctor($doctorId) {

		return DB::table('diplome_cin')
			->where('doctor_id', '=', $doctorId)
			->first();
	}
}
